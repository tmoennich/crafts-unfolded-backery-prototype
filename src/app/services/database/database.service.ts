import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { map, switchMap } from 'rxjs/operators';
import { combineLatest, Observable, of } from 'rxjs';

const offerCollectionName = 'offer';
const offerImagesCollectionName = 'offerImages';
const orderCollectionName = 'order';

type DatabaseModel<T> = T & {
  id: string;
};

export interface OfferModel {
  userId: string;
  name: string;
  description: string;
  imageId: string;
  imageUrl: string;
}

export interface OrderModel {
  userId: string;
  offerId: string;
  amount: number;
  orderDate: Date;
}

export interface OrderWithOfferModel extends OrderModel {
  offer: OfferModel;
}

export interface OfferImageFileModel {
  name: string;
  filepath: string;
  size: number;
}

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private offerCollection: AngularFirestoreCollection<DatabaseModel<OfferModel>>;
  private offerImageCollection: AngularFirestoreCollection<DatabaseModel<OfferImageFileModel>>;
  private orderCollection: AngularFirestoreCollection<DatabaseModel<OrderModel>>;

  public constructor(
    private angularFirestore: AngularFirestore,
  ) {
    this.offerCollection = angularFirestore.collection<DatabaseModel<OfferModel>>(offerCollectionName);
    this.offerImageCollection = angularFirestore.collection<DatabaseModel<OfferImageFileModel>>(offerImagesCollectionName);
    this.orderCollection = angularFirestore.collection<DatabaseModel<OrderModel>>(orderCollectionName);
  }

  getAllOffer(userId: string) {
    return this.offerCollection.ref
      .where('userId', '==', userId);
  }

  getAllOrder(userId: string) {
    return this.orderCollection.ref
      .where('userId', '==', userId);
  }

  getAllOrderWithOffer(userId: string): Observable<OrderWithOfferModel[]> {
    return this.angularFirestore
      .collection<DatabaseModel<OrderModel>>(
        orderCollectionName,
        ref => ref.where('userId', '==', userId)
      )
      .valueChanges()
      .pipe(
        switchMap(orders => {
          const offerIdList = Array.from(new Set(orders.map(order => order.offerId)).values());

          return combineLatest([
            of(orders),
            combineLatest(
              offerIdList.map(offerId =>
                this.angularFirestore
                  .collection<DatabaseModel<OfferModel>>(
                    offerCollectionName,
                    ref => ref.where('id', '==', offerId)
                  )
                  .valueChanges()
                  .pipe(
                    map(authors => authors[0])
                  )
              )
            )
          ]);
        }),
        map(([orders, offers]) => orders.map(order => ({
          ...order,
          offer: offers.find(a => a.id === order.offerId)
        })))
      );
  }

  createOffer(data: OfferModel) {
    const offerId = this.angularFirestore.createId();

    return this.offerCollection
      .doc(offerId)
      .set({
        ...data,
        id: offerId,
      });
  }

  createOfferImageId() {
    return this.angularFirestore.createId();
  }

  createOfferImage(imageId: string, image: OfferImageFileModel) {

    return this.offerImageCollection
      .doc(imageId)
      .set({
        ...image,
        id: imageId,
      });
  }
}
