import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, Platform, PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { AuthService } from './services/authentication/auth.service';
import firebase from 'firebase/compat/app';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  user: firebase.User;

  constructor(
    private router: Router,
    private platform: Platform,
    private storage: Storage,
    private authService: AuthService,
    private menu: MenuController,
    private popoverCtrl: PopoverController,
    private firebaseAuth: AngularFireAuth,
  ) {
    this.initializeApp();
  }

  ngOnInit() {
    return this.authService.getUserState()
      .subscribe(user => {
        this.user = user;
      });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // TODO: May require to refresh token?
      this.firebaseAuth.onAuthStateChanged(user => {
        if (!user) {
          this.router.navigateByUrl('login');
        }
      });

      this.storage.create();
    });
  }

  logOut() {
    this.authService.logout();
    this.menu.enable(false, 'main-menu');
    this.menu.close('main-menu');
  }

  public closeMenu() {
    this.menu.close('main-menu');
  }
}
