import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/authentication/auth.service';
import { MenuController } from '@ionic/angular';
import { DatabaseService } from 'src/app/services/database/database.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public amountOffers: number;
  public amountOrders: number;

  constructor(
    private router: Router,
    private authService: AuthService,
    private menu: MenuController,
    private databaseService: DatabaseService,
  ) {
  }

  ngOnInit() {
    return this.authService.getUserState()
      .subscribe(({ uid }) => {

        this.databaseService.getAllOffer(uid)
          .onSnapshot(snap => {
            this.amountOffers = snap.docs.length;
          });
        this.databaseService.getAllOrder(uid)
          .onSnapshot(snap => {
            this.amountOrders = snap.docs.length;
          });
      });
  }

  openSideNav() {
    this.menu.enable(true, 'main-menu');
    this.menu.open('main-menu');
  }

  public goToOffers() {
    this.router.navigateByUrl('/offers');
  }

  public goToOrders() {
    this.router.navigateByUrl('/orders');
  }
}
