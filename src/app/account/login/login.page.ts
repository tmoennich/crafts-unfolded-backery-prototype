import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/authentication/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit, OnDestroy {
  authError: any;

  private subscriptions = new Subscription();

  constructor(private router: Router, private authService: AuthService) {
  }

  ngOnInit() {
    const sub = this.authService.eventAuthError$.subscribe(data => {
      this.authError = data;
    });
    this.subscriptions.add(sub);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  login() {
    return this.authService.login();
  }

}
