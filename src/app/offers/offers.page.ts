import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DatabaseService, OfferModel } from 'src/app/services/database/database.service';
import { AuthService } from 'src/app/services/authentication/auth.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit, OnDestroy {
  public offers: OfferModel[];

  private subscriptions = new Subscription();

  constructor(
    private router: Router,
    private databaseService: DatabaseService,
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.subscriptions.add(this.authService.getUserState()
      .subscribe(({ uid }) => {

        this.databaseService.getAllOffer(uid)
          .onSnapshot(snap => {
            this.offers = snap.docs.map(d => d.data());
            console.log('Offers', { offers: this.offers });
          });
      }));
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  public formatDescription(description: string) {
    return description.replace(/\r\n|\r|\n/g, '<br />');
  }
}
