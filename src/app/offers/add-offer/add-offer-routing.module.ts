import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddOfferPage } from 'src/app/offers/add-offer/add-offer.page';

const routes: Routes = [
  {
    path: '',
    component: AddOfferPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddOfferPageRoutingModule {
}
