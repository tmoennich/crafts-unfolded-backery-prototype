import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPaginationModule } from 'ngx-pagination';
import { AddOfferPage } from 'src/app/offers/add-offer/add-offer.page';
import { AddOfferPageRoutingModule } from 'src/app/offers/add-offer/add-offer-routing.module';
import { FormatFileSizePipe } from 'src/app/pipes/format-file-size.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    AddOfferPageRoutingModule,
    NgxPaginationModule,
    ReactiveFormsModule
  ],
  declarations: [
    AddOfferPage,
    FormatFileSizePipe,
  ]
})
export class AddOfferPageModule {
}
