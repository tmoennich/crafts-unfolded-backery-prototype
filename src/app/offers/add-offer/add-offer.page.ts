import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/compat/storage';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import firebase from 'firebase/compat/app';
import { AuthService } from 'src/app/services/authentication/auth.service';
import { ToastController } from '@ionic/angular';
import { Color } from '@ionic/core';
import { DatabaseService, OfferImageFileModel, OfferModel } from 'src/app/services/database/database.service';

@Component({
  selector: 'app-add-offer',
  templateUrl: './add-offer.page.html',
  styleUrls: ['./add-offer.page.scss'],
})
export class AddOfferPage implements OnInit {
  public formGroup: FormGroup;
  // File upload task
  public fileUploadTask: AngularFireUploadTask;
  // Upload progress
  public percentageVal: Observable<number>;
  // Uploaded File URL
  public uploadedImageUrl: Observable<string>;
  // Image specifications
  public imgName: string;
  public imgSize: number;
  // File uploading status
  public isFileUploading: boolean;
  public isFileUploaded: boolean;

  private uploadedImageFileId: string;
  private user: firebase.User;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private angularFirestore: AngularFirestore,
    private angularFireStorage: AngularFireStorage,
    private authService: AuthService,
    private toastController: ToastController,
    private databaseService: DatabaseService,
  ) {
    this.formGroup = this.formBuilder
      .group({
        name: ['', Validators.required],
        description: ['', Validators.required],
        // Won't be used in database, but blocks the submit until an image has been provided
        image: ['', Validators.required],
      });

    this.isFileUploading = false;
    this.isFileUploaded = false;
  }

  ngOnInit() {
    return this.authService.getUserState()
      .subscribe(user => {
        this.user = user;
      });
  }

  uploadImage(event: FileList) {
    const file = event.item(0);

    // Image validation
    if (file.type.split('/')[0] !== 'image') {
      console.log('File type is not supported!');
      return;
    }

    this.isFileUploading = true;
    this.isFileUploaded = false;
    this.imgName = file.name;

    // Storage path
    const fileStoragePath = `productImages/${new Date().getTime()}_${file.name}`;
    // Image reference
    const imageRef = this.angularFireStorage.ref(fileStoragePath);
    // File upload task
    this.fileUploadTask = this.angularFireStorage.upload(fileStoragePath, file);
    this.fileUploadTask.catch(error => {
      console.log('Upload error', error);
    });
    // Show uploading progress
    this.percentageVal = this.fileUploadTask.percentageChanges();
    this.fileUploadTask.snapshotChanges()
      .subscribe(snapshot => {
        this.imgSize = snapshot.totalBytes;

        console.log({ snapshot });

        if (snapshot.state !== 'success') {
          return;
        }

        // Retrieve uploaded image storage path
        this.uploadedImageUrl = imageRef.getDownloadURL();

        this.uploadedImageUrl.subscribe(uploadedImagePath => {
          console.log({ uploadedImagePath, file });
          this.storeOfferImage({
            name: file.name,
            filepath: uploadedImagePath,
            size: this.imgSize
          });

          this.isFileUploading = false;
          this.isFileUploaded = true;
        }, error => {
          console.log({ error });
        });
      }, error => {
        console.log({ error });
      });
  }

  submitForm() {

    this.uploadedImageUrl.subscribe(imageUrl => {

      const offerData: OfferModel = {
        name: this.formGroup.value.name,
        description: this.formGroup.value.description,
        userId: this.user.uid,
        imageId: this.uploadedImageFileId,
        imageUrl,
      };

      this.databaseService
        .createOffer(offerData)
        .then(() => {
          this.presentToast('Angebot erfolg hinzugefügt!', 'success');

          this.router.navigateByUrl('/offers');
        })
        .catch(err => {
          console.log('Failed to store offer', { err });
        });
    });
  }

  storeOfferImage(image: OfferImageFileModel) {
    this.uploadedImageFileId = this.databaseService.createOfferImageId();

    this.databaseService.createOfferImage(this.uploadedImageFileId, image)
      .then(res => {
        console.log('Image stored', { res });
      })
      .catch(err => {
        console.log('Failed to store image', { err });
      });
  }

  async presentToast(message: string, color: Color, duration = 2000) {
    const toast = await this.toastController.create({
      message,
      duration,
      color
    });

    toast.present();
  }
}
