import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { OffersPageRoutingModule } from 'src/app/offers/offers-routing.module';
import { OffersPage } from 'src/app/offers/offers.page';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OffersPageRoutingModule,
    TranslateModule,
    NgxPaginationModule
  ],
  declarations: [OffersPage]
})
export class OffersPageModule {
}
