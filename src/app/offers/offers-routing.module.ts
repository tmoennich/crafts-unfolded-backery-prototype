import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OffersPage } from 'src/app/offers/offers.page';

const routes: Routes = [
  {
    path: '',
    component: OffersPage
  },
  {
    path: 'add',
    loadChildren: () => import('./add-offer/add-offer.module').then(m => m.AddOfferPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OffersPageRoutingModule {
}
