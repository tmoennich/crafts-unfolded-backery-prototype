import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { DatabaseService, OrderWithOfferModel } from 'src/app/services/database/database.service';
import { AuthService } from 'src/app/services/authentication/auth.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  public orders = new Array<OrderWithOfferModel>();

  private subscriptions = new Subscription();

  constructor(
    private router: Router,
    private databaseService: DatabaseService,
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    this.subscriptions.add(this.authService.getUserState()
      .subscribe(({ uid }) => {
        this.databaseService.getAllOrderWithOffer(uid)
          .subscribe(orders => {
            this.orders = orders;
          });
      }));
  }

}
