# Backery Prototype App

Umsetzung eines Prototypes der Backery App.

Ziel ist es, Angebote einer Bäckerei zu erfassen und diese gleichzeitig mit Bestellungen
für die Angebote zu versorgen.

Side-Quest: Angular weniger hassen.

_Spoiler: Mission failed.._

## Developer Journey

### Tag 1 - Dienstag, 08.03.22

- Suche nach einem Angular Ionic Firebase Template/Starter
- Suche nach einem Angular Ionic Firebase Template/Starter
- Suche nach einem Angular Ionic Firebase Template/Starter
- ...

Ich habe 6 oder 7 GitHub Projekte ausprobiert, die mir zwar Beispiele für einige Konzepte lieferten, aber letztlich für meinen Zweck
ungeeignet waren.
Mal absolut besch..eidener Code Style, mal totaler Spaghetti Code.

Ernüchterndes Ergebnis nach Tag 1: Ich muss ein nacktes Ionic Angular Firebase Basis Template nutzen und alles selbst bauen.

### Tag 2 - Mittwoch, 09.03.22

- Fork eines aktuellen Ionic Starters
- Konfiguration und Einrichtung von Firebase
- Grob Konzeption der App
  - Startseite mit simpler Übersicht von Angeboten und Bestellungen
  - Falls Zeit ist, Hinweis wann die nächste Bestellung geliefert werden muss
  - 2 Bereiche
    - Bereich 1: Angebote
      - Auflistung inserierter Angebote
      - Falls Zeit ist, löschen & bearbeiten
      - Button um Angebot anzulegen
      - Seite um Angebot anzulegen
      - Felder: Titel, Beschreibung, Bild
    - Bereich 2: Bestellungen
      - Auflistung bestellter Angebote mit Anzahl
      - Falls Zeit ist, fake/debug Menü zum hinzufügen einer Bestellung aus der App
- Generierung der einzelnen Seiten mittels Angular CLI
- Recherche Google Login via Firebase
- Implementierung Google Login
- Implementierung Formular zur Anlage eines Angebots
- Troubleshooting Image Upload

### Tag 3 - Donnerstag, 10.03.22

- Abschluss Anlage eines Angebots mit Bild Upload
- Implementierung Auflistung inserierter Angebote
- Modelierung Datenbank für Bestellungen
- Implementierung Auflistung vorhandener Bestellungen
- Implementierung Hinweise auf Startseite
- einfache Dokumentation & Dairy

## Datenbank Screenshots

Da ich nicht weiß, wie cih das Schema der Daten aus Firestore exportiert bekomme, hier 2 Beispiel Screenshots der Daten.

#### Angebote

![offer db](./docs/firebase-db-offer.png)

#### Bestellungen

![order db](./docs/firebase-db-order.png)
